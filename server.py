import shutil
import requests
import os
from flask import Flask, request, jsonify
from uber import decode_image
import boto3
from scan import scan

app = Flask(__name__)

AUTH_TOKEN=''
S3_KEY=''
CALLBACK_URL=''
# BUCKET_NAME=''
USER_EMAIL=''
BUCKET_NAME='idfy-pcc-live'
AWS_ACCESS_KEY_ID='AKIAJMCGUQJGH34HMB6A'
AWS_SECRET_ACCESS_KEY='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'


@app.route('/image', methods=['POST'])
def download_image():
    url =request.args.get('image_loc')
    response = requests.get(url, stream=True)
    with open('img.jpg', 'wb') as out_file:
        shutil.copyfileobj(response.raw, out_file)
    del response
    input_file = open('input_file', 'w')
    input_file.write('img.jpg 5:1')
    input_file.close()
    # scan('s3_image.jpg', 'img.jpg')
    ret_json = decode_image()
    return jsonify(ret_json)

@app.route('/pcc_call', methods=['POST'])
def get_pcc_data():
    pcc_json = request.json 
    print pcc_json
    # return jsonify(request.json)
    global AUTH_TOKEN, S3_KEY, CALLBACK_URL, BUCKET_NAME, USER_EMAIL
    
    AUTH_TOKEN = pcc_json['authenticity_token']
    S3_KEY = pcc_json['attachment_path'] 
    CALLBACK_URL = pcc_json['call_back']
    try:
        if pcc_json['bucket']:
            bucket = pcc_json['bucket']
    except:
        bucket = BUCKET_NAME
    try:
        if pcc_json['aws_access_key_id']:
            aws_access_key_id = pcc_json['aws_access_key_id']
    except:
        aws_access_key_id = AWS_ACCESS_KEY_ID
    try:
        if pcc_json['aws_secret_access_key']:
            aws_secret_access_key = pcc_json['aws_secret_access_key']
    except:
        aws_secret_access_key = AWS_SECRET_ACCESS_KEY
    USER_EMAIL = pcc_json['user_email']

    # print "*"*40
    # print S3_KEY
    # print "*"*40
    
    S3_KEY = S3_KEY.encode('ascii', 'ignore')
    aws_access_key_id = aws_access_key_id.encode('ascii', 'ignore')
    aws_secret_access_key = aws_secret_access_key.encode('ascii', 'ignore')
    bucket = bucket.encode('ascii', 'ignore')

    s3_client = boto3.client('s3',aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)
    data = open('img.jpg', 'wb')
    print '*'*50
    print bucket
    print S3_KEY
    print aws_secret_access_key
    print aws_access_key_id
    print '*'*50
    s3_client.download_fileobj(bucket, S3_KEY, data)
    data.close()
    input_file = open('input_file', 'w')
    input_file.write('img.jpg 5:1')
    input_file.close()
    # scan('s3_image.jpg', 'img.jpg')
    ret_json = decode_image()
    # if ret_json == "failed":
    #     input_file = open('input_file', 'w')
    #     input_file.write('s3_image.jpg 5:1')
    #     input_file.close()
    #     ret_json = decode_image()
    # print "*"*50
    # print "ret_json"
    # print ret_json
    # print "*"*50
    return jsonify(ret_json)


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    # app.run(host='0.0.0.0', port=port)
    app.run(host='0.0.0.0', port=port,debug=True)