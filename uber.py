from generatejson import create_output
import requests
import json
import re
import string



def decode_image():

    input_file = open('input_file', 'r')
    create_output(input_file, "output.json")
    data = open("output.json").read()

    response = requests.post(url='https://vision.googleapis.com/v1/images:annotate?key=AIzaSyCYZXQtrJOQDWjAab66LBkjQWNG-HEMw9c',
        data=data,
        headers={'Content-Type': 'application/json'})

    try:
        ocr_text = json.loads(response.text)['responses'][0]['textAnnotations'][0]['description']
    except KeyError:
        return "failed"
    # print "*"*50
    # print ocr_text
    # print "*"*50
    try:
        ocr_text = ocr_text.encode('ascii', 'ignore')    
    except UnicodeEncodeError:
        ocr_text

    ocr_text2 = "".join(ocr_text.split())
    # print ocr_text
    # print '*'*50
    # print type(ocr_text)
    # print '*'*50

    if 'SCHEDULE' in ocr_text2.upper():
        # print 'yolo'
        ret_json = get_ocr(ocr_text)
    else:
        ret_json = get_ocr_dl(ocr_text)

    return ret_json

def get_ocr_dl(in_text):
    in_text = in_text.upper()
    arr = in_text.split('\n')
    ret_data = {}
    name = ''
    fathers_name = ''
    dob = ''
    address = ''
    dl_no = ''
    for index,tag in enumerate(arr):
        if tag == 'NAME':
            name_pre = arr[index-1]
            if 'SEE' in name_pre:
                name = arr[index+1]
            else:
                name = arr[index-1]
            ret_data['name'] = name
        if 'LMV' in tag:
            fathers_name = arr[index+1]
            ret_data['fathers_name'] = fathers_name
        if 'ADDRESS' in tag:
            address += tag
            idx = index+1
            while True:
                if 'SIGN' in arr[idx]:
                    break
                else:
                    address += arr[idx]
                    idx += 1
            address = address.replace('ADDRESS', '')
            ret_data['address'] = address
        if 'NO. KA' in tag:
            dl_no += tag
            dl_no = dl_no.replace('SL', 'DL')
            ret_data['dl_no'] = dl_no

    ret_data['dob'] = ''
    ret_data['textAnnotation'] = in_text
    ret_data = json.dumps(ret_data)

    return ret_data

def get_ocr(txt_arr):
    txt_arr = txt_arr.upper()
    arr = txt_arr.split('\n')
    ret_data = {}
    split_blr = False
    split_blru = False
    no_more_txt = False
    flag = 0
    beg = 0
    tag_txt = ''
    print arr
    for index,line in enumerate(arr):
        if 'BANGALORE' in line:
            flag = index
            split_blr = True
            break
        if 'BENGALURU' in line:
            flag = index 
            split_blru = True
            break
        if ' AT ' in line:
            beg = index
            tag_txt = 'AT '
            print 'line1',line, tag_txt
        if 'AT ' in line:
            beg = index
            tag_txt = 'AT '
            print 'line2', line, tag_txt
        if 'AT' in line:
            beg = index
            tag_txt = 'AT '
            print 'line0',line, tag_txt
        if 'PARCEL ' in line:
            beg = index 
            tag_txt = 'PARCEL '
        if 'BEARING ' in line:
            beg = index 
            tag_txt = 'BEARING '

    blr_text = arr[flag] 
    if split_blr:
        blr_text = blr_text.split('BANGALORE', 1)[0]
        blr_text = blr_text + 'BANGALORE'
    if split_blru:
        blr_text = blr_text.split('BENGALURU', 1)[0]
        blr_text = blr_text + 'BENGALURU'
    arr[flag] = blr_text

    if tag_txt:
        if beg < flag:
            tag_break_txt = arr[beg]
            try:
                tag_break_txt = tag_break_txt.split(tag_txt,1)[1]
            except IndexError as e:
                no_more_txt = True
            if not no_more_txt:
                arr[beg] = tag_break_txt
            else:
                arr[beg] = ''
            arr_mod = arr[beg:flag+1]
            address = string.join(arr_mod, ' ')
            ret_data['addr'] = address

    # print arr
    print flag
    print beg
    print tag_txt
    # addr_txt = arr[flag-1]
    # ret_data['addr'] = addr_txt
    # ret_data['blr'] = blr_text

    return ret_data

def get_ocr_lease(txt_arr):
    txt_arr = txt_arr.upper()
    arr = txt_arr.split('\n')
    ret_data = {}
    split_blr = False
    split_blru = False
    # get number of schedules in text first
    # print 'inside lease l#82'
    # print txt_arr
    print arr
    # print 'txt_arr'
    # print txt_arr
    # sum = 0
    schedule_index = []
    for index,line in enumerate(arr):
        if 'SCHEDULE' in line:
            # sum += 1
            schedule_index.append(index)
            # print schedule_index, "scheduledyo"

    #text post schedule
    arr_mod =[]
    tag_txt = ''    
    blore_found = False
    while not blore_found:    
        print "scheduledyolo", schedule_index
        print '*',tag_txt,'*',blore_found
        arr_mod = arr[schedule_index[-1]:len(arr)]
        beg = 0
        last = 0
        for index,line in enumerate(arr_mod):
            if ' AT ' in line:
                beg = index
                tag_txt = 'AT '
            if 'AT ' in line:
                beg = index
                tag_txt = 'AT '
            if 'PARCEL ' in line:
                beg = index 
                tag_txt = 'PARCEL '
            if 'BEARING ' in line:
                beg = index 
                tag_txt = 'BEARING '
            if 'BANGALORE' in line:
                if tag_txt: 
                    last = index
                    split_blr = True
                    blore_found = True
                    print blore_found, "*", last
                break
            if 'BENGALURU' in line:
                if tag_txt:
                    last = index
                    split_blru = True
                    blore_found = True
                    print blore_found, "*", last
                break
        print "****",tag_txt,"***"
        if not blore_found:
            schedule_index.pop()
    # print '*'*10
    # print tag_txt
    # print '*'*10

    # line_after_city = arr_mod[last+1]
    # line_before_city = arr_mod[last-1]

    tag_break_txt = arr_mod[beg]
    tag_break_txt = tag_break_txt.split(tag_txt,1)[1]
    arr_mod[beg] = tag_break_txt
    blr_text = arr_mod[last]
    # line_before_city = tag_break_txt
    # line_after_city = blr_text.split('BANGALORE', 1)[1]
    if split_blr:
        blr_text = blr_text.split('BANGALORE', 1)[0]
        blr_text = blr_text + 'BANGALORE'
    if split_blru:
        blr_text = blr_text.split('BENGALURU', 1)[0]
        blr_text = blr_text + 'BENGALURU'

    split_blr, split_blru = False, False
    # blr_strip = arr_mod[last]
    # blr_strip = blr_strip.split('-')
    # blr_strip_index = 0
    # for index,tag in enumerate(blr_strip):
        # if 'BANGALORE' in tag:  
            # blr_strip_index = index
    # arr_mod[last] = string.join(blr_strip[0:blr_strip_index+1], ' ')
    arr_mod[last] = blr_text
    arr_mod = arr_mod[beg:last+1]
    address = string.join(arr_mod, ' ')
    # print '*'*10
    # print address
    # print '*'*10
    ret_data['primary_response'] = address
    # ret_data['secondary_response'] = {}
    # ret_data['secondary_response']['before_city'] = line_before_city
    # ret_data['secondary_response']['after_city'] = line_after_city
    ret_data = json.dumps(ret_data)
    return ret_data

if __name__ == '__main__':
    decode_image()

